# Report usage
usage() {
  echo "Usage:"
  echo "$(basename $0) -h                     Help"
  echo "               -l || --link           Git link"
  echo "               -p || --port           Port"
  echo "               -s || --server         Server"
  echo "               --dbhost               Database Hostname"
  echo "               -d || --dbname         Database Name"
  echo "               --dbu                  Database Username"
  echo "               --dbpass               Database Password"

  # Optionally exit with a status code
  if [ -n "$1" ]; then
    exit "$1"
  fi
}

invalid() {
  echo "Unrecognized format: $1" >&2
  usage 1
}

# Pre-process options to:
# - expand -xyz into -x -y -z
# - expand --longopt=arg into --longopt arg
ARGV=()
END_OF_OPT=
while [[ $# -gt 0 ]]; do
  arg="$1"
  shift
  case "${END_OF_OPT}${arg}" in
  --)
    ARGV+=("$arg")
    END_OF_OPT=1
    ;;
  --*=*) ARGV+=("${arg%%=*}" "${arg#*=}") ;;
  --*)
    ARGV+=("$arg")
    END_OF_OPT=1
    ;;
  -*) for i in $(seq 2 ${#arg}); do ARGV+=("-${arg:i-1:1}"); done ;;
  *) ARGV+=("$arg") ;;
  esac
done

# Apply pre-processed options
set -- "${ARGV[@]}"

# Parse options
END_OF_OPT=
POSITIONAL=()
while [[ $# -gt 0 ]]; do
  case "${END_OF_OPT}${1}" in
  -h | --help) usage 0 ;;
  -d | --dbname)
    shift
    database="$1"
    ;;
  --dbhost)
    shift
    hostname="$1"
    ;;
  --dbu)
    shift
    username="$1"
    ;;
  --dbpass)
    shift
    password="$1"
    ;;
  -p | --port)
    shift
    port="$1"
    ;;
  -l | --link)
    shift
    link="$1"
    ;;
  -s | --server)
    shift
    server="$1"
    ;;
  --) END_OF_OPT=1 ;;
  -*) invalid "$1" ;;
  *) POSITIONAL+=("$1") ;;
  esac
  shift
done

# Restore positional parameters
set -- "${POSITIONAL[@]}"

if [ -z "$link" ]; then
  read -p  "Enter GIT repository link:" link
  # echo "Git link not entered"
  # echo "Use -h or --help for help"
  # echo "Exited"
  # exit
fi

git clone $link GitFile
cd GitFile

sudo virtualenv -p python3 env
source env/bin/activate
sudo ./env/bin/pip3 install -r requirements.txt

if [ -z "$server" ]; then
  read -p "Enter Server name: " server
  # echo "Sever name needed"
  # echo "Use -h or --help for help"
  # echo "Exited"
  # exit
fi

if [ -z "$port" ]; then
  read -p "Enter Port: " port
  # echo "Port needed"
  # echo "Use -h or --help for help"
  # echo "Exited"
  # exit
fi
while [lsof -Pi :$port -sTCP:LISTEN -t >/dev/null]; do
  if lsof -Pi :$port -sTCP:LISTEN -t >/dev/null; then
    echo "ERROR"
    echo "PORT $port already in use "
    read -p "Enter another port: " port
  # echo "Exited"
  # exit
  else
    break
  fi
done
sed -i 's/^PORT =.*$/PORT = '$port'/g' config.py

echo "server {
            listen 8080;

            server_name $server;

            location / {
                proxy_pass http://localhost:$port/;
                }
         }" >nginx.conf

if [ -z "$dbhost"]; then
  read -p "Enter a Database connection URL" dbhost
  # echo "Database hostname needed"
  # echo "Use -h or --help for help"
  # echo "Exited"
  # exit
fi
sed -i 's/^DB_URI = .*$/DB_URI = \"mongodb:\/\/'$hostname'\"/g' config.py
if [ -z "$username" ]; then
  read "Enter a Database connection username" username
  # echo "Database Username needed"
  # echo "Use -h or --help for help"
  # echo "Exited"
  # exit
fi
sed -i 's/^DB_USERNAME =.*$/DB_USERNAME = \"'$username'\"/g' config.py
if [ -z "$password" ]; then
  read -p "Enter Database connection password" password
  # echo "Database Password needed"
  # echo "Use -h or --help for help"
  # echo "Exited"
  # exit
fi
sed -i 's/^DB_PASSWORD =.*$/DB_PASSWORD = \"'$password'\"/g' config.py
if [ -z "$database" ]; then
  read -p "Enter a Database name" database
  # echo "Database name needed"
  # echo "Use -h or --help for help"
  # echo "Exited"
  # exit
fi
sed -i 's/^DB_NAME .*$=/DB_NAME = \"'$database'\"/g' config.py
sudo cp nginx.conf /etc/nginx/sites-available/nginx.conf
sudo ln -s /etc/nginx/sites-available/nginx.conf /etc/nginx/sites-enabled/nginx.conf
uwsgi --socket 0.0.0.0:$port --protocol=http -w wsgi:app -d true

sudo service nginx restart
